const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  server: {
    port: PORT['dtp-text-validator'][ENV]
  }
};
